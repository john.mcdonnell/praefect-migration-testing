--
-- PostgreSQL database dump
--

-- Dumped from database version 11.14 (Debian 11.14-1.pgdg90+1)
-- Dumped by pg_dump version 11.14 (Debian 11.14-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: replication_job_state; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.replication_job_state AS ENUM (
    'ready',
    'in_progress',
    'completed',
    'cancelled',
    'failed',
    'dead'
);


ALTER TYPE public.replication_job_state OWNER TO postgres;

--
-- Name: notify_on_change(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.notify_on_change() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
				DECLARE
					msg JSONB;
				BEGIN
				    	CASE TG_OP
					WHEN 'INSERT' THEN
						SELECT JSON_AGG(obj) INTO msg
						FROM (
							SELECT JSONB_BUILD_OBJECT('virtual_storage', virtual_storage, 'relative_paths', ARRAY_AGG(DISTINCT relative_path)) AS obj
							FROM NEW
							GROUP BY virtual_storage
						) t;
					WHEN 'UPDATE' THEN
						SELECT JSON_AGG(obj) INTO msg
						FROM (
							SELECT JSONB_BUILD_OBJECT('virtual_storage', virtual_storage, 'relative_paths', ARRAY_AGG(DISTINCT relative_path)) AS obj
							FROM NEW
							FULL JOIN OLD USING (virtual_storage, relative_path)
							GROUP BY virtual_storage
						) t;
					WHEN 'DELETE' THEN
						SELECT JSON_AGG(obj) INTO msg
						FROM (
							SELECT JSONB_BUILD_OBJECT('virtual_storage', virtual_storage, 'relative_paths', ARRAY_AGG(DISTINCT relative_path)) AS obj
							FROM OLD
							GROUP BY virtual_storage
						) t;
					END CASE;

				    	CASE WHEN JSONB_ARRAY_LENGTH(msg) > 0 THEN
						PERFORM PG_NOTIFY(TG_ARGV[TG_NARGS-1], msg::TEXT);
					ELSE END CASE;

					RETURN NULL;
				END;
				$$;


ALTER FUNCTION public.notify_on_change() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: node_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.node_status (
    id bigint NOT NULL,
    praefect_name character varying(511) NOT NULL,
    shard_name character varying(255) NOT NULL,
    node_name character varying(255) NOT NULL,
    last_contact_attempt_at timestamp with time zone,
    last_seen_active_at timestamp with time zone
);


ALTER TABLE public.node_status OWNER TO postgres;

--
-- Name: healthy_storages; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.healthy_storages AS
 SELECT ns.shard_name AS virtual_storage,
    ns.node_name AS storage
   FROM public.node_status ns
  WHERE (ns.last_seen_active_at >= (now() - '00:00:10'::interval))
  GROUP BY ns.shard_name, ns.node_name
 HAVING ((count(ns.praefect_name))::numeric >= ( SELECT ceil(((count(DISTINCT node_status.praefect_name))::numeric / 2.0)) AS quorum_count
           FROM public.node_status
          WHERE (((node_status.shard_name)::text = (ns.shard_name)::text) AND (node_status.last_contact_attempt_at >= (now() - '00:01:00'::interval)))))
  ORDER BY ns.shard_name, ns.node_name;


ALTER TABLE public.healthy_storages OWNER TO postgres;

--
-- Name: hello_world; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hello_world (
    id integer
);


ALTER TABLE public.hello_world OWNER TO postgres;

--
-- Name: node_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.node_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.node_status_id_seq OWNER TO postgres;

--
-- Name: node_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.node_status_id_seq OWNED BY public.node_status.id;


--
-- Name: replication_queue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.replication_queue (
    id bigint NOT NULL,
    state public.replication_job_state DEFAULT 'ready'::public.replication_job_state NOT NULL,
    created_at timestamp without time zone DEFAULT timezone('UTC'::text, now()) NOT NULL,
    updated_at timestamp without time zone,
    attempt integer DEFAULT 3 NOT NULL,
    lock_id text,
    job jsonb,
    meta jsonb
);


ALTER TABLE public.replication_queue OWNER TO postgres;

--
-- Name: replication_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.replication_queue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.replication_queue_id_seq OWNER TO postgres;

--
-- Name: replication_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.replication_queue_id_seq OWNED BY public.replication_queue.id;


--
-- Name: replication_queue_job_lock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.replication_queue_job_lock (
    job_id bigint NOT NULL,
    lock_id text NOT NULL,
    triggered_at timestamp without time zone DEFAULT timezone('UTC'::text, now()) NOT NULL
);


ALTER TABLE public.replication_queue_job_lock OWNER TO postgres;

--
-- Name: replication_queue_lock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.replication_queue_lock (
    id text NOT NULL,
    acquired boolean DEFAULT false NOT NULL
);


ALTER TABLE public.replication_queue_lock OWNER TO postgres;

--
-- Name: repositories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.repositories (
    virtual_storage text NOT NULL,
    relative_path text NOT NULL,
    generation bigint,
    "primary" text,
    repository_id bigint NOT NULL,
    replica_path text
);


ALTER TABLE public.repositories OWNER TO postgres;

--
-- Name: repositories_repository_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.repositories_repository_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.repositories_repository_id_seq OWNER TO postgres;

--
-- Name: repositories_repository_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.repositories_repository_id_seq OWNED BY public.repositories.repository_id;


--
-- Name: repository_assignments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.repository_assignments (
    virtual_storage text NOT NULL,
    relative_path text NOT NULL,
    storage text NOT NULL,
    repository_id bigint
);


ALTER TABLE public.repository_assignments OWNER TO postgres;

--
-- Name: storage_repositories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.storage_repositories (
    virtual_storage text NOT NULL,
    relative_path text NOT NULL,
    storage text NOT NULL,
    generation bigint NOT NULL,
    repository_id bigint
);


ALTER TABLE public.storage_repositories OWNER TO postgres;

--
-- Name: repository_generations; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.repository_generations AS
 SELECT storage_repositories.virtual_storage,
    storage_repositories.relative_path,
    max(storage_repositories.generation) AS generation
   FROM public.storage_repositories
  GROUP BY storage_repositories.virtual_storage, storage_repositories.relative_path;


ALTER TABLE public.repository_generations OWNER TO postgres;

--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schema_migrations (
    id text NOT NULL,
    applied_at timestamp with time zone
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: shard_primaries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shard_primaries (
    id bigint NOT NULL,
    shard_name character varying(255) NOT NULL,
    node_name character varying(255) NOT NULL,
    elected_by_praefect character varying(255) NOT NULL,
    elected_at timestamp with time zone NOT NULL,
    read_only boolean DEFAULT false NOT NULL,
    demoted boolean DEFAULT false NOT NULL,
    previous_writable_primary text
);


ALTER TABLE public.shard_primaries OWNER TO postgres;

--
-- Name: shard_primaries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.shard_primaries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shard_primaries_id_seq OWNER TO postgres;

--
-- Name: shard_primaries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.shard_primaries_id_seq OWNED BY public.shard_primaries.id;


--
-- Name: valid_primaries; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.valid_primaries AS
 SELECT candidates.virtual_storage,
    candidates.relative_path,
    candidates.storage
   FROM ( SELECT storage_repositories.virtual_storage,
            storage_repositories.relative_path,
            storage_repositories.storage,
            ((repository_assignments.storage IS NOT NULL) OR bool_and((repository_assignments.storage IS NULL)) OVER (PARTITION BY storage_repositories.virtual_storage, storage_repositories.relative_path)) AS eligible
           FROM (((public.storage_repositories
             JOIN public.repository_generations USING (virtual_storage, relative_path, generation))
             JOIN public.healthy_storages USING (virtual_storage, storage))
             LEFT JOIN public.repository_assignments USING (virtual_storage, relative_path, storage))
          WHERE (NOT (EXISTS ( SELECT
                   FROM public.replication_queue
                  WHERE ((replication_queue.state <> ALL (ARRAY['completed'::public.replication_job_state, 'dead'::public.replication_job_state, 'cancelled'::public.replication_job_state])) AND ((replication_queue.job ->> 'change'::text) = 'delete_replica'::text) AND ((replication_queue.job ->> 'virtual_storage'::text) = storage_repositories.virtual_storage) AND ((replication_queue.job ->> 'relative_path'::text) = storage_repositories.relative_path) AND ((replication_queue.job ->> 'target_node_storage'::text) = storage_repositories.storage)))))) candidates
  WHERE candidates.eligible;


ALTER TABLE public.valid_primaries OWNER TO postgres;

--
-- Name: virtual_storages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.virtual_storages (
    virtual_storage text NOT NULL,
    repositories_imported boolean DEFAULT false NOT NULL
);


ALTER TABLE public.virtual_storages OWNER TO postgres;

--
-- Name: node_status id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_status ALTER COLUMN id SET DEFAULT nextval('public.node_status_id_seq'::regclass);


--
-- Name: replication_queue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.replication_queue ALTER COLUMN id SET DEFAULT nextval('public.replication_queue_id_seq'::regclass);


--
-- Name: repositories repository_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.repositories ALTER COLUMN repository_id SET DEFAULT nextval('public.repositories_repository_id_seq'::regclass);


--
-- Name: shard_primaries id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shard_primaries ALTER COLUMN id SET DEFAULT nextval('public.shard_primaries_id_seq'::regclass);


--
-- Data for Name: hello_world; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.hello_world (id) FROM stdin;
1
\.


--
-- Data for Name: node_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.node_status (id, praefect_name, shard_name, node_name, last_contact_attempt_at, last_seen_active_at) FROM stdin;
1	praefect.test:0.0.0.0:2305	default	gitaly1	2022-01-13 04:37:37.550272+00	2022-01-13 04:37:37.550272+00
2	praefect.test:0.0.0.0:2305	default	gitaly2	2022-01-13 04:37:37.550272+00	2022-01-13 04:37:37.550272+00
3	praefect.test:0.0.0.0:2305	default	gitaly3	2022-01-13 04:37:37.550272+00	2022-01-13 04:37:37.550272+00
\.


--
-- Data for Name: replication_queue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.replication_queue (id, state, created_at, updated_at, attempt, lock_id, job, meta) FROM stdin;
\.


--
-- Data for Name: replication_queue_job_lock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.replication_queue_job_lock (job_id, lock_id, triggered_at) FROM stdin;
\.


--
-- Data for Name: replication_queue_lock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.replication_queue_lock (id, acquired) FROM stdin;
default|gitaly2|@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.wiki.git	f
default|gitaly3|@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.wiki.git	f
default|gitaly2|@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.wiki.git	f
default|gitaly2|@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.git	f
default|gitaly2|@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.wiki.git	f
default|gitaly3|@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.git	f
default|gitaly3|@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.wiki.git	f
default|gitaly3|@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.git	f
default|gitaly3|@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.wiki.git	f
default|gitaly3|@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.git	f
default|gitaly1|@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.wiki.git	f
default|gitaly1|@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.git	f
default|gitaly1|@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.wiki.git	f
default|gitaly1|@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.git	f
default|gitaly1|@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.git	f
default|gitaly1|@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.wiki.git	f
default|gitaly2|@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.git	f
default|gitaly3|@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.wiki.git	f
default|gitaly2|@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.wiki.git	f
default|gitaly2|@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.git	f
default|gitaly1|@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.git	f
default|gitaly1|@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.wiki.git	f
default|gitaly1|@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.git	f
default|gitaly1|@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.wiki.git	f
default|gitaly2|@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.wiki.git	f
default|gitaly2|@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.git	f
default|gitaly3|@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.wiki.git	f
default|gitaly3|@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.git	f
default|gitaly3|@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.git	f
default|gitaly2|@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.wiki.git	f
default|gitaly3|@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.wiki.git	f
default|gitaly3|@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.git	f
default|gitaly1|@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.wiki.git	f
default|gitaly1|@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.git	f
default|gitaly2|@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.git	f
default|gitaly3|@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.wiki.git	f
default|gitaly2|@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.wiki.git	f
default|gitaly2|@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.git	f
default|gitaly1|@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.wiki.git	f
default|gitaly3|@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.git	f
default|gitaly1|@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.git	f
default|gitaly1|@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.wiki.git	f
\.


--
-- Data for Name: repositories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.repositories (virtual_storage, relative_path, generation, "primary", repository_id, replica_path) FROM stdin;
default	@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.git	1	gitaly3	1	@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.git
default	@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.wiki.git	1	gitaly1	2	@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.wiki.git
default	@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.wiki.git	1	gitaly2	4	@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.wiki.git
default	@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.git	4	gitaly2	3	@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.git
default	@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.wiki.git	1	gitaly1	12	@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.wiki.git
default	@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.wiki.git	1	gitaly2	6	@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.wiki.git
default	@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.git	4	gitaly3	5	@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.git
default	@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.git	4	gitaly1	11	@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.git
default	@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.wiki.git	1	gitaly3	8	@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.wiki.git
default	@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.wiki.git	1	gitaly3	18	@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.wiki.git
default	@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.git	4	gitaly3	7	@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.git
default	@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.wiki.git	1	gitaly3	10	@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.wiki.git
default	@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.wiki.git	1	gitaly1	14	@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.wiki.git
default	@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.git	4	gitaly2	9	@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.git
default	@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.wiki.git	1	gitaly2	22	@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.wiki.git
default	@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.git	4	gitaly1	13	@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.git
default	@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.git	4	gitaly2	17	@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.git
default	@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.wiki.git	1	gitaly2	16	@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.wiki.git
default	@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.git	4	gitaly3	15	@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.git
default	@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.wiki.git	1	gitaly3	20	@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.wiki.git
default	@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.git	4	gitaly2	21	@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.git
default	@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.git	4	gitaly1	19	@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.git
\.


--
-- Data for Name: repository_assignments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.repository_assignments (virtual_storage, relative_path, storage, repository_id) FROM stdin;
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.schema_migrations (id, applied_at) FROM stdin;
20200109161404_hello_world	2022-01-13 04:26:07.462093+00
20200113151438_1_test_migration	2022-01-13 04:26:07.46385+00
20200224220728_job_queue	2022-01-13 04:26:07.478987+00
20200225220728_readd_job_queue	2022-01-13 04:26:07.482655+00
20200324001604_add_sql_election_tables	2022-01-13 04:26:07.496841+00
20200422131451_add_shard_read_only_column	2022-01-13 04:26:07.498704+00
20200512131219_replication_job_indexing	2022-01-13 04:26:07.501967+00
20200527103816_drop_old_gitaly_tables	2022-01-13 04:26:07.503988+00
20200602154246_remember_previous_writable_primary	2022-01-13 04:26:07.505549+00
20200707101830_repositories_table	2022-01-13 04:26:07.513722+00
20200810055650_replication_queue_cleanup	2022-01-13 04:26:07.516072+00
20200921154417_repositories_nullable_generation	2022-01-13 04:26:07.517531+00
20200921170311_repositories_primary_column	2022-01-13 04:26:07.519096+00
20201006125956_trigger_repository_update_generation	2022-01-13 04:26:07.522051+00
20201102115118_variable_replication_factor	2022-01-13 04:26:07.52384+00
20201102171914_virtual_storage_configuration	2022-01-13 04:26:07.52872+00
20201126165633_repository_assignments_table	2022-01-13 04:26:07.543475+00
20201208163237_cleanup_notifiactions_payload	2022-01-13 04:26:07.545648+00
20201231075619_remove_unused_assigned_column	2022-01-13 04:26:07.547608+00
20210223130233_delete_replica_unique_index	2022-01-13 04:26:07.552553+00
20210225101159_replication_queue_target_index	2022-01-13 04:26:07.556641+00
20210525143540_healthy_storages_view	2022-01-13 04:26:07.559377+00
20210525173505_valid_primaries_view	2022-01-13 04:26:07.563111+00
20210607124235_optimize_valid_primaries_view	2022-01-13 04:26:07.56565+00
20210727085659_repository_ids	2022-01-13 04:26:07.578973+00
20210906130405_add_replica_path	2022-01-13 04:26:07.581146+00
\.


--
-- Data for Name: shard_primaries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.shard_primaries (id, shard_name, node_name, elected_by_praefect, elected_at, read_only, demoted, previous_writable_primary) FROM stdin;
\.


--
-- Data for Name: storage_repositories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.storage_repositories (virtual_storage, relative_path, storage, generation, repository_id) FROM stdin;
default	@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.git	gitaly1	1	1
default	@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.git	gitaly2	1	1
default	@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.git	gitaly3	1	1
default	@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.wiki.git	gitaly1	1	2
default	@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.wiki.git	gitaly2	1	2
default	@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.wiki.git	gitaly3	1	2
default	@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.wiki.git	gitaly1	1	14
default	@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.git	gitaly2	4	9
default	@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.wiki.git	gitaly2	1	10
default	@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.wiki.git	gitaly2	1	4
default	@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.wiki.git	gitaly3	1	8
default	@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.git	gitaly2	4	3
default	@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.wiki.git	gitaly3	1	4
default	@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.git	gitaly3	4	3
default	@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.wiki.git	gitaly1	1	4
default	@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.git	gitaly1	4	3
default	@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.git	gitaly3	4	9
default	@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.wiki.git	gitaly1	1	10
default	@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.git	gitaly1	4	9
default	@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.wiki.git	gitaly2	1	6
default	@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.git	gitaly3	4	7
default	@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.wiki.git	gitaly1	1	8
default	@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.git	gitaly1	4	7
default	@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.git	gitaly3	4	5
default	@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.wiki.git	gitaly1	1	6
default	@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.git	gitaly1	4	5
default	@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.git	gitaly2	4	5
default	@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.wiki.git	gitaly3	1	6
default	@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.wiki.git	gitaly2	1	8
default	@hashed/4b/22/4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a.git	gitaly2	4	7
default	@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.git	gitaly1	4	11
default	@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.wiki.git	gitaly3	1	10
default	@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.wiki.git	gitaly2	1	12
default	@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.git	gitaly2	4	11
default	@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.wiki.git	gitaly3	1	12
default	@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.git	gitaly3	4	11
default	@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.wiki.git	gitaly1	1	12
default	@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.wiki.git	gitaly2	1	16
default	@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.git	gitaly3	4	15
default	@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.git	gitaly2	4	13
default	@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.wiki.git	gitaly3	1	14
default	@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.git	gitaly3	4	13
default	@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.git	gitaly1	4	13
default	@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.wiki.git	gitaly2	1	14
default	@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.wiki.git	gitaly1	1	16
default	@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.git	gitaly1	4	15
default	@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.git	gitaly2	4	15
default	@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.wiki.git	gitaly3	1	16
default	@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.git	gitaly3	4	17
default	@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.git	gitaly2	4	17
default	@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.git	gitaly1	4	17
default	@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.wiki.git	gitaly3	1	20
default	@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.wiki.git	gitaly2	1	20
default	@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.wiki.git	gitaly1	1	20
default	@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.git	gitaly1	4	19
default	@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.git	gitaly2	4	19
default	@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.git	gitaly3	4	19
default	@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.wiki.git	gitaly2	1	22
default	@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.git	gitaly2	4	21
default	@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.wiki.git	gitaly3	1	22
default	@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.git	gitaly3	4	21
default	@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.wiki.git	gitaly1	1	22
default	@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.git	gitaly1	4	21
default	@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.wiki.git	gitaly3	1	18
default	@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.wiki.git	gitaly1	1	18
default	@hashed/19/58/19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7.wiki.git	gitaly2	1	18
\.


--
-- Data for Name: virtual_storages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.virtual_storages (virtual_storage, repositories_imported) FROM stdin;
\.


--
-- Name: node_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.node_status_id_seq', 2058, true);


--
-- Name: replication_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.replication_queue_id_seq', 82, true);


--
-- Name: repositories_repository_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.repositories_repository_id_seq', 22, true);


--
-- Name: shard_primaries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.shard_primaries_id_seq', 1, false);


--
-- Name: node_status node_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_status
    ADD CONSTRAINT node_status_pkey PRIMARY KEY (id);


--
-- Name: replication_queue_job_lock replication_queue_job_lock_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.replication_queue_job_lock
    ADD CONSTRAINT replication_queue_job_lock_pk PRIMARY KEY (job_id, lock_id);


--
-- Name: replication_queue_lock replication_queue_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.replication_queue_lock
    ADD CONSTRAINT replication_queue_lock_pkey PRIMARY KEY (id);


--
-- Name: replication_queue replication_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.replication_queue
    ADD CONSTRAINT replication_queue_pkey PRIMARY KEY (id);


--
-- Name: repositories repositories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.repositories
    ADD CONSTRAINT repositories_pkey PRIMARY KEY (repository_id);


--
-- Name: repository_assignments repository_assignments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.repository_assignments
    ADD CONSTRAINT repository_assignments_pkey PRIMARY KEY (virtual_storage, relative_path, storage);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (id);


--
-- Name: shard_primaries shard_primaries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shard_primaries
    ADD CONSTRAINT shard_primaries_pkey PRIMARY KEY (id);


--
-- Name: storage_repositories storage_repositories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.storage_repositories
    ADD CONSTRAINT storage_repositories_pkey PRIMARY KEY (virtual_storage, relative_path, storage);


--
-- Name: virtual_storages virtual_storages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.virtual_storages
    ADD CONSTRAINT virtual_storages_pkey PRIMARY KEY (virtual_storage);


--
-- Name: delete_replica_unique_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX delete_replica_unique_index ON public.replication_queue USING btree (((job ->> 'virtual_storage'::text)), ((job ->> 'relative_path'::text))) WHERE ((state <> ALL (ARRAY['completed'::public.replication_job_state, 'cancelled'::public.replication_job_state, 'dead'::public.replication_job_state])) AND ((job ->> 'change'::text) = 'delete_replica'::text));


--
-- Name: replication_queue_target_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX replication_queue_target_index ON public.replication_queue USING btree (((job ->> 'virtual_storage'::text)), ((job ->> 'relative_path'::text)), ((job ->> 'target_node_storage'::text)), ((job ->> 'change'::text))) WHERE (state <> ALL (ARRAY['completed'::public.replication_job_state, 'cancelled'::public.replication_job_state, 'dead'::public.replication_job_state]));


--
-- Name: repository_lookup_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX repository_lookup_index ON public.repositories USING btree (virtual_storage, relative_path);


--
-- Name: shard_name_on_node_status_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX shard_name_on_node_status_idx ON public.node_status USING btree (shard_name, node_name);


--
-- Name: shard_name_on_shard_primaries_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX shard_name_on_shard_primaries_idx ON public.shard_primaries USING btree (shard_name);


--
-- Name: shard_node_names_on_node_status_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX shard_node_names_on_node_status_idx ON public.node_status USING btree (praefect_name, shard_name, node_name);


--
-- Name: virtual_target_on_replication_queue_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX virtual_target_on_replication_queue_idx ON public.replication_queue USING btree (((job ->> 'virtual_storage'::text)), ((job ->> 'target_node_storage'::text)));


--
-- Name: repositories notify_on_delete; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER notify_on_delete AFTER DELETE ON public.repositories REFERENCING OLD TABLE AS old FOR EACH STATEMENT EXECUTE PROCEDURE public.notify_on_change('repositories_updates');


--
-- Name: storage_repositories notify_on_delete; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER notify_on_delete AFTER DELETE ON public.storage_repositories REFERENCING OLD TABLE AS old FOR EACH STATEMENT EXECUTE PROCEDURE public.notify_on_change('storage_repositories_updates');


--
-- Name: storage_repositories notify_on_insert; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER notify_on_insert AFTER INSERT ON public.storage_repositories REFERENCING NEW TABLE AS new FOR EACH STATEMENT EXECUTE PROCEDURE public.notify_on_change('storage_repositories_updates');


--
-- Name: storage_repositories notify_on_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER notify_on_update AFTER UPDATE ON public.storage_repositories REFERENCING OLD TABLE AS old NEW TABLE AS new FOR EACH STATEMENT EXECUTE PROCEDURE public.notify_on_change('storage_repositories_updates');


--
-- Name: replication_queue_job_lock replication_queue_job_lock_job_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.replication_queue_job_lock
    ADD CONSTRAINT replication_queue_job_lock_job_id_fkey FOREIGN KEY (job_id) REFERENCES public.replication_queue(id);


--
-- Name: replication_queue_job_lock replication_queue_job_lock_lock_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.replication_queue_job_lock
    ADD CONSTRAINT replication_queue_job_lock_lock_id_fkey FOREIGN KEY (lock_id) REFERENCES public.replication_queue_lock(id);


--
-- Name: repository_assignments repository_assignments_repository_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.repository_assignments
    ADD CONSTRAINT repository_assignments_repository_id_fkey FOREIGN KEY (repository_id) REFERENCES public.repositories(repository_id) ON DELETE CASCADE;


--
-- Name: repository_assignments repository_assignments_virtual_storage_relative_path_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.repository_assignments
    ADD CONSTRAINT repository_assignments_virtual_storage_relative_path_fkey FOREIGN KEY (virtual_storage, relative_path) REFERENCES public.repositories(virtual_storage, relative_path) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: storage_repositories storage_repositories_repository_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.storage_repositories
    ADD CONSTRAINT storage_repositories_repository_id_fkey FOREIGN KEY (repository_id) REFERENCES public.repositories(repository_id) ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--

